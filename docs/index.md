# MazeGeek BD, Khulna R&D Team

This is a summary website for **MazeGeek BD, Khulna Research & Development Team**
For GitLab Repository visit [mazegeek](https://gitlab.com/tanmoy_MazeGeek/Vehicle_Detection)

## Goals

* `Vehicle Detection Dataset` - Around 10k raw images taken from phone, public domian images
* `Research Paper on Vehicle Detection` - Open Dataset, Model Architecture + Accuracy, Online Learning
* `US Car Number Plate Recognition [State wise]` - Dataset + Constructing solution for each states 
* `VIN Dataset` - Minimum 2k VIN samples
* `Automated VIN Detection System` - Research TO DO

## Timeline

    Phase 1    #    {To be decided}
    
    Phase 2    #    {To be decided}
        ...    #  [Yet to be decided]
